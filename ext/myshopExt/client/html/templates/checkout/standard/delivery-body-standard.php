<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Metaways Infosystems GmbH, 2013
 * @copyright Aimeos (aimeos.org), 2015-2020
 */

$enc = $this->encoder();
?>
<?php $this->block()->start('checkout/standard/delivery'); ?>
<section class="checkout-standard-delivery">

    <h1><?= $enc->html($this->translate('client', 'delivery'), $enc::TRUST); ?></h1>
    <p class="note"><?= $enc->html($this->translate('client', 'Please choose your delivery method'), $enc::TRUST); ?></p>

    <table class="table">
        <tr>
            <th><?= $enc->html($this->translate('client', 'Supplier'), $enc::TRUST); ?></th>
            <th><?= $enc->html($this->translate('client', 'delivery'), $enc::TRUST); ?></th>
            <th><?= $enc->html($this->translate('client', 'Pick-up'), $enc::TRUST); ?></th>
        </tr>
        <?php foreach ($this->get("supplierNames") as $supplierID => $supplierName): ?>
            <tr>
                <td><?= $supplierName ?></td>
                <td>
                    <div class="col-sm-6">
                        <input class="option" type="radio"
                               id="c_deliveryoption-<?= $enc->attr($this->formparam([$supplierID, "c_deliveryoption",
                                $this->get("supplierDeliveries")[$supplierID]->get("service.id")])); ?>"
                               name="<?= $enc->attr($this->formparam(['c_deliveryoption',
                                   $supplierID])); ?>"
                               value="<?= $enc->attr($this->get("supplierDeliveries")[$supplierID]->get("service.id")); ?>"
                        />
                        <?= number_format($this->get("deliveriesPrices")[$supplierID],
                            2, '.', ''); ?> <?= $this->get("standardBasket")->getPrice()->get("price.currencyid") ?>
                    </div>
                </td>
                <td>
                    <div class="col-sm-6">
                        <input class="option" type="radio"
                        id="c_deliveryoption-<?= $enc->attr($this->formparam([$supplierID, "c_deliveryoption",
                            $this->get("deliveriesPickup")[$supplierID]->get("service.id")])); ?>"
                               name="<?= $enc->attr($this->formparam(['c_deliveryoption',
                                   $supplierID])); ?>"
                               value="<?= $enc->attr($this->get("deliveriesPickup")[$supplierID]->get("service.id")); ?>"
                        />
                        0 <?= $this->get("standardBasket")->getPrice()->get("price.currencyid") ?></td>
                </div>
            </tr>
        <?php endforeach; ?>
    </table>

    <div class="button-group">
        <a class="btn btn-default btn-lg btn-back" href="<?= $enc->attr($this->get('standardUrlBack')); ?>">
            <?= $enc->html($this->translate('client', 'Previous'), $enc::TRUST); ?>
        </a>
        <button class="btn btn-primary btn-lg btn-action">
            <?= $enc->html($this->translate('client', 'Next'), $enc::TRUST); ?>
        </button>
    </div>

</section>
<?php $this->block()->stop(); ?>
<?= $this->block()->get('checkout/standard/delivery'); ?>

<?php
$enc = $this->encoder();
$nodes = $this->treeCatalogTree->getChildren();
?>
<section class="aimeos catalog-list" style="display: flex; flex-wrap: wrap">
    <div class=" catalog-list-items">
        <ul class="list-items">
            <?php foreach ($nodes as $item) : ?>
                <li class="product"
                    itemtype="http://schema.org/Supplier"
                    itemscope="">

                    <a class="media-list" href=<?= "/shop/" . $item->get("url") . "~" . $item->getId() ?>>
                        <?php $mediaItem = $item->getRefItems('media', 'default', 'default')->first(); ?>
                        <?php if (!($mediaItem === null)): ?>
                            <div class="media-item" itemscope="" itemtype="http://schema.org/ImageObject">
                                <img src="<?= $enc->attr($this->content($mediaItem->getPreview())); ?>"
                                     alt="<?= $enc->attr($mediaItem->getName()); ?>"/>
                                <meta itemprop="contentUrl"
                                      content="<?= $enc->attr($this->content($mediaItem->getPreview())); ?>"/>
                            </div>
                        <?php endif; ?>

                    </a>
                    <div class="text-list">
                        <h2 itemprop="name"><?= $enc->html($item->getName(), $enc::TRUST); ?></h2>
                        <?php foreach ($item->getRefItems('text', 'short', 'default') as $textItem) : ?>
                            <div class="text-item" itemprop="description">
                                <?= $enc->html($textItem->getContent(), $enc::TRUST); ?><br/>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>

<?php

$enc = $this->encoder();

?>

<head>

    <link rel="stylesheet" href="/css/Contacts/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/Contacts/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="/css/Contacts/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/css/Contacts/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="/css/Contacts/nice-select.css" type="text/css">
    <link rel="stylesheet" href="/css/Contacts/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="/css/Contacts/style.css" type="text/css">
</head>

<body>
<!-- Breadcrumb Section Begin -->
<section class="breadcrumb-section set-bg" data-setbg="/files/Contacts.jpg"
         style="background-image: url('/files/Contacts.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2><?= $this->translate('client', 'About us'); ?></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="about-section">
    <p style="margin-left: 5em; margin-right: 5em; font-size: larger;font-family: 'icomoon'">
        <?= $this->translate('client', 'About us text'); ?>
        <br><br>
        <?= $this->translate('client', 'About us mision'); ?>
        <br>
        <?= $this->translate('client', 'About us vision'); ?>
    </p>
    <h2 style="text-align:center !important;"><?= $this->translate('client', 'Our team'); ?></h2>
        <div class="row" style="width: 60%;margin-left: 20%">
            <div class="column">
                <div class="card">
                    <img src="files/ValtersSlava.png" alt="Valters" style="width:50%; margin-left: 25%">
                    <div class="container">
                        <h2>Valters Slava</h2>
                        <p class="title">CEO</p>
                    </div>
                </div>
            </div>

            <div class="column">
                <div class="card">
                    <img src="files/ArtursOlekss.png" alt="Arturs" style="width:50%; margin-left: 25%">
                    <div class="container">
                        <h2>Artūrs Olekšs</h2>
                        <p class="title">CTO</p>
                    </div>
                </div>
            </div>
        </div>
</div>

<style>

    *, *:before, *:after {
        box-sizing: border-box;
    }

    .about-section {
        padding: 2em;
        text-align: center;
    }

    .column {
        width: 50%;
        padding: 0 8px;
    }

    .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }

    @media screen and (max-width: 650px) {
        .column {
            width: 100%;
            display: block;
        }
    }
</style>

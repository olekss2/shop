<?php
$enc = $this->encoder();
?>
<section class="aimeos catalog-list" style="display: flex; flex-wrap: wrap">
    <div class=" catalog-list-items">
        <ul class="list-items">
            <?php foreach ($this->suppliers as $id => $supplier): ?>
                <li class="product"
                    itemtype="http://schema.org/Supplier"
                    itemscope="">
                    <table>
                        <tr>
                            <td>
                                <a class="media-list" href=<?= "/products?f_supid" . urlencode("[0]") . "=" . $id ?>>
                                    <?php $mediaItem = $supplier->getRefItems('media', 'default', 'default')->first(); ?>
                                    <?php if (!($mediaItem === null)): ?>
                                        <div class="media-item" itemscope="" itemtype="http://schema.org/ImageObject">
                                            <img src="<?= $enc->attr($this->content($mediaItem->getPreview())); ?>"
                                                 alt="<?= $enc->attr($mediaItem->getName()); ?>"/>
                                            <meta itemprop="contentUrl"
                                                  content="<?= $enc->attr($this->content($mediaItem->getPreview())); ?>"/>
                                        </div>
                                    <?php endif; ?>

                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="text-list">
                                    <h2 itemprop="name"><?= $enc->html($supplier->getName(), $enc::TRUST); ?></h2>
                                    <?php foreach ($supplier->getRefItems('text', 'short', 'default') as $textItem) : ?>
                                        <div class="text-item" itemprop="description">
                                            <?= $enc->html($textItem->getContent(), $enc::TRUST); ?><br/>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </td>
                        <tr/>
                        <tr>
                            <td>
                                <?php if ($this->supplierMinFreeDelivery[$supplier->getCode()] != "-"): ?>
                                    <?= $enc->html($this->translate('client', 'Free shipping starting from '), $enc::TRUST); ?>
                                    <b><?= $this->supplierMinFreeDelivery[$supplier->getCode()] ?> <?= $this->currencySup[$supplier->getCode()] ?>
                                    </b>
                                <?php else: ?>
                                    <div style="color: red">
                                        <?= $enc->html($this->translate('client', 'Free shipping is not available'), $enc::TRUST); ?>
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= $enc->html($this->translate('client', 'Categories'), $enc::TRUST); ?>
                                :<b> <?= implode(",", $this->supplierCategories[$supplier->getCode()]) ?> </b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?= $enc->html($this->translate('client', 'Number of products'), $enc::TRUST); ?>
                                : <b><?= $this->supplierProductCount[$supplier->getCode()] ?></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href=<?= "/products?f_supid" . urlencode("[0]") . "=" . $id ?>>
                                    <button class="btn btn-primary">
                                        <?= $enc->html($this->translate('client', 'Go to shop'), $enc::TRUST); ?>
                                    </button>
                            </td>
                        </tr>
                    </table>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>

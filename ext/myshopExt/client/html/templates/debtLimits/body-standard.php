<?php
$enc = $this->encoder();
$listTarget = $this->config('client/html/debtLimits/url/target');
$listController = $this->config('client/html/debtLimits/url/controller', 'changeprices');
$listAction = $this->config('client/html/debtLimits/url/action', 'list');
$listConfig = $this->config('client/html/debtLimits/url/config', []);
$infiniteScroll = $this->config('client/html/debtLimits/infinite-scroll', false);
$debtLimits = $this->get('debtLimits');

if (isset($_POST["save"]) &&
    !(\Illuminate\Support\Facades\Auth::guest())) {
    $input = $this->request()->getParsedBody();
    unset($input["_token"]);
    unset($input["save"]);

    $supplierID = \Illuminate\Support\Facades\Auth::user()->id;
    $currentTimeStamp = \Carbon\Carbon::now()->toDateTimeString();

    foreach ($input as $customerID => $val):
        if (substr($customerID, -5) === "_decr") {

            if ($val <= 0)
                continue;

            $customerID = substr($customerID, 0, -5);
            $debt = floatval($debtLimits[$customerID]->debt) - floatval($val);

            $lineKey = [];
            $lineVal = [];
            $lineKey['customer_id'] = $customerID;
            $lineKey['supplier_id'] = $supplierID;
            $lineVal['debt'] = $debt;
            $lineVal['updated_at'] = $currentTimeStamp;

            DB::table('debts')
                ->updateOrInsert($lineKey, $lineVal);
            $debtLimits[$customerID]->debt = $debt;

        } else if (!($val === $debtLimits[$customerID]->limit)) {
            $lineKey = [];
            $lineVal = [];
            $lineKey['customer_id'] = $customerID;
            $lineKey['supplier_id'] = $supplierID;
            $lineVal['limit'] = floatval($val);
            $lineVal['updated_at'] = $currentTimeStamp;
            DB::table('debt_limits')
                ->updateOrInsert($lineKey, $lineVal);
            $debtLimits[$customerID]->limit = $val;
        }
    endforeach;
}

?>
<section
    class="aimeos">

    <?php if (isset($this->listErrorList)) : ?>
        <ul class="error-list">
            <?php foreach ((array)$this->listErrorList as $errmsg) : ?>
                <li class="error-item"><?= $enc->html($errmsg); ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

    <form method="POST">
        <?= $this->csrf()->formfield(); ?>
        <div class="catalog-list-items"
             data-infinite-url="<?= $infiniteScroll && $this->get('listPageNext', 0) >
             $this->get('listPageCurr', 0) ? $this->url($listTarget, $listController,
                 $listAction, array('l_page' => $this->get('listPageNext')) +
                 $this->get('listParams', []), [], $listConfig) : '' ?>">

            <h2><?= $enc->html($this->translate('client', 'Debt Limits'), $enc::TRUST); ?></h2>

            <table style="width:100%">
                <tr>
                    <th><?= $enc->html($this->translate('client', 'Customer'), $enc::TRUST); ?></th>
                    <th><?= $enc->html($this->translate('client', 'Debt Limit'), $enc::TRUST); ?></th>
                    <th><?= $enc->html($this->translate('client', 'Current debt'), $enc::TRUST); ?></th>
                    <th><?= $enc->html($this->translate('client', 'Reduce debt'), $enc::TRUST); ?></th>
                </tr>

                <?php foreach ($debtLimits as $debtLimit) : ?>
                    <tr>
                        <th><?= $debtLimit->name ?></th>
                        <th>
                            <input type="number" step="0.01"
                                   name=<?= $debtLimit->id ?> value= <?= $debtLimit->limit === null ? 0 : $debtLimit->limit ?>>
                        </th>
                        <th>
                            <?= $debtLimit->debt === null ? 0 : $debtLimit->debt ?>
                        </th>
                        <th>
                            <input type="number" step="0.01"
                                   name=<?= $debtLimit->id . "_decr" ?> value= 0/>
                        </th>
                    </tr>
                <?php endforeach; ?>

            </table>
            <div align="center">
                <button class="btn btn-primary btn-lg" type="submit" name="save" value="save">
                    <?= $enc->html($this->translate('client', 'Save'), $enc::TRUST); ?>
                </button>
            </div>
        </div>
    </form>

</section>

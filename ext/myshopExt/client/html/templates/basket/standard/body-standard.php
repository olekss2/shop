<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Metaways Infosystems GmbH, 2012
 * @copyright Aimeos (aimeos.org), 2015-2020
 */

use Illuminate\Support\Facades\DB;

$enc = $this->encoder();

$basketTarget = $this->config('client/html/basket/standard/url/target');
$basketController = $this->config('client/html/basket/standard/url/controller', 'basket');
$basketAction = $this->config('client/html/basket/standard/url/action', 'index');
$basketConfig = $this->config('client/html/basket/standard/url/config', []);

$checkoutTarget = $this->config('client/html/checkout/standard/url/target');
$checkoutController = $this->config('client/html/checkout/standard/url/controller', 'checkout');
$checkoutAction = $this->config('client/html/checkout/standard/url/action', 'index');
$checkoutConfig = $this->config('client/html/checkout/standard/url/config', []);

$optTarget = $this->config('client/jsonapi/url/target');
$optCntl = $this->config('client/jsonapi/url/controller', 'jsonapi');
$optAction = $this->config('client/jsonapi/url/action', 'options');
$optConfig = $this->config('client/jsonapi/url/config', []);

if (isset($_POST["saveToOrderTemplates"]) && !(Auth::user() === null)) {
    saveToOrderTemplates($this->standardBasket, Auth::user()->id);
}
?>
<section class="aimeos basket-standard"
         data-jsonurl="<?= $enc->attr($this->url($optTarget, $optCntl, $optAction, [], [], $optConfig)); ?>">

    <?php if (isset($this->standardErrorList)) : ?>
        <ul class="error-list">
            <?php foreach ((array)$this->standardErrorList as $errmsg) : ?>
                <li class="error-item"><?= $this->translate('client', $errmsg); ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>



    <?php if (isset($this->standardBasket)) : ?>

        <?php if (!(\Illuminate\Support\Facades\App::getLocale() === 'lv')): ?>
            <h1><?= $enc->html($this->translate('client', 'Basket'), $enc::TRUST); ?></h1>
        <?php else: ?>
            <div class="aimeosLV">
                <h1><?= $enc->html($this->translate('client', 'Basket'), $enc::TRUST); ?></h1>
            </div>
        <?php endif; ?>

        <form method="POST"
              action="<?= $enc->attr($this->url($basketTarget, $basketController, $basketAction, [], [], $basketConfig)); ?>">
            <?= $this->csrf()->formfield(); ?>


            <div class="common-summary-detail">
                <div class="header">

                    <?php if (!(\Illuminate\Support\Facades\App::getLocale() === 'lv')): ?>
                        <h2><?= $enc->html($this->translate('client', 'Details'), $enc::TRUST); ?></h2>
                    <?php else: ?>
                        <div class="aimeosLV">
                            <h2><?= $enc->html($this->translate('client', 'Details'), $enc::TRUST); ?></h2>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="basket table-responsive">
                    <?= $this->partial(
                    /** client/html/basket/standard/summary/detail
                     * Location of the detail partial template for the basket standard component
                     *
                     * To configure an alternative template for the detail partial, you
                     * have to configure its path relative to the template directory
                     * (usually client/html/templates/). It's then used to display the
                     * product detail block in the basket standard component.
                     *
                     * @param string Relative path to the detail partial
                     * @since 2017.01
                     * @category Developer
                     */
                        $this->config('client/html/basket/standard/summary/detail', 'common/summary/detail-standard'),
                        array(
                            'summaryEnableModify' => true,
                            'summaryBasket' => $this->standardBasket,
                            'summaryTaxRates' => $this->get('standardTaxRates', []),
                            'summaryNamedTaxes' => $this->get('standardNamedTaxes', []),
                            'summaryErrorCodes' => $this->get('standardErrorCodes', []),
                            'summaryCostsPayment' => $this->get('standardCostsPayment', 0),
                            'summaryCostsDelivery' => $this->get('standardCostsDelivery', 0),
                            'productsSuppliers' => $this->get('productsSuppliers'),
                            'supplierNames' => $this->get('supplierNames'),
                            'supplierTotalPrice' => $this->get('supplierTotalPrice'),
                            'supplierTotalPriceTax' => $this->get('supplierTotalPriceTax'),
                            'supplierDeliveries' => $this->get('supplierDeliveries'),
                            'deliveriesPrices' => $this->get('deliveriesPrices'),
                            'supplierTotalDeliveryTax' => $this->get('supplierTotalDeliveryTax')
                        )
                    ); ?>
                </div>
            </div>

            <div class="button-group">

                <?php if (isset($this->standardBackUrl)) : ?>
                    <a class="btn btn-default btn-lg btn-back" href="<?= $enc->attr($this->standardBackUrl); ?>">
                        <?= $enc->html($this->translate('client', 'Back'), $enc::TRUST); ?>
                    </a>
                <?php endif; ?>

                <button class="btn btn-default btn-lg btn-update" type="submit">
                    <?= $enc->html($this->translate('client', 'Update'), $enc::TRUST); ?>
                </button>

                <?php if ($this->get('standardCheckout', false) === true) : ?>
                    <a class="btn btn-primary btn-lg btn-action"
                       href="<?= $enc->attr($this->url($checkoutTarget, $checkoutController, $checkoutAction, [], [], $checkoutConfig)); ?>">
                        <?= $enc->html($this->translate('client', 'Checkout'), $enc::TRUST); ?>
                    </a>
                <?php else : ?>
                    <input type="hidden" name="<?= $enc->attr($this->formparam('b_action')) ?>" value="1"/>
                    <button class="btn btn-primary btn-lg btn-action" type="submit">
                        <?= $enc->html($this->translate('client', 'Check'), $enc::TRUST); ?>
                    </button>
                <?php endif; ?>

            </div>
        </form>
        <form method="POST">
            <?= $this->csrf()->formfield(); ?>
            <div class="button-group">
                <button class="btn btn-primary btn-lg btn-action" type="submit"
                        name="saveToOrderTemplates" value="save">
                    <?= $enc->html($this->translate('client', 'Add to Order templates'), $enc::TRUST); ?>
                </button>
            </div>
        </form>

    <?php endif; ?>

</section>

<?php
function saveToOrderTemplates($basket, $customer)
{
    $currentTimeStamp = \Carbon\Carbon::now()->toDateTimeString();
    $id = DB::table('order_templates')->insertGetId(["customer_id" => $customer,
        "created_at" => $currentTimeStamp]);
    $newEntries = [];
    foreach ($basket->getProducts() as $position => $product) :
        $quantity = $product->getQuantity();
        $productID = $product->getProductId();
        array_push($newEntries, ["id" => $id, "product_id" => $productID,
            "quantity" => intval($quantity)]);
    endforeach;
    DB::table('order_templates_content')->insert($newEntries);
}

?>

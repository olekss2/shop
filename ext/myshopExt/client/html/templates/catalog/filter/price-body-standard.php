<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Aimeos (aimeos.org), 2020
 */

$enc = $this->encoder();

$target = $this->config('client/html/catalog/lists/url/target');
$cntl = $this->config('client/html/catalog/lists/url/controller', 'catalog');
$action = $this->config('client/html/catalog/lists/url/action', 'list');
$config = $this->config('client/html/catalog/lists/url/config', []);

$categoryManager = $this->get('categoryManager');
$filterParams = $this->get('filterParams');
$uom = '';

if (array_key_exists('f_catid', $filterParams)) {
    $id = $filterParams['f_catid'];
    $category = $categoryManager->getItem($id);
    $categoryConfig = $category->get('config');
    if (array_key_exists('UOF', $categoryConfig)) {
        $uom = $categoryConfig['UOF'];
    }
}

?>
<?php $this->block()->start('catalog/filter/price'); ?>
<?php if ($this->get('priceHigh', 0)) : ?>
    <section class="catalog-filter-price col">
        <h2><img src="/files/icons/price_icon.png"
                 height="20"> <?= $enc->html($this->translate('client', 'Price'), $enc::TRUST); ?></h2>

        <div class="price-lists">
            <?php if ($this->param('f_price')) : ?>
                <a class="btn btn-secondary"
                   href="<?= $enc->attr($this->url($target, $cntl, $action, $this->get('priceResetParams', []), [], $config)); ?>">
                    <?= $enc->html($this->translate('client', 'Reset')) ?>
                </a>
            <?php endif ?>

            <fieldset>
                <div class="price-input">
                    <p style="text-align:left;">
                        <?= $enc->html($this->translate('client', 'From')) ?>
                        <span style="float:right;">
                        <?= $enc->html($this->translate('client', 'To')) ?>
                    </span>
                    </p>
                    <input type="number" class="price-low" name="<?= $this->formparam(['f_price', 0]) ?>"
                           min="0" max="<?= $enc->html($this->get('priceHigh', 0)) ?>" step="1"
                           value="<?= $enc->html($this->param('f_price/0', 0)) ?>">
                    <input type="number" class="price-high" name="<?= $this->formparam(['f_price', 1]) ?>"
                           min="0" max="<?= $enc->html($this->get('priceHigh', 0)) ?>" step="1"
                           value="<?= $enc->html($this->param('f_price/1', $this->get('priceHigh', 0))) ?>">
                    <?php if (!($uom === '')): ?>
                        <?= 'EUR/' . $uom ?>
                    <?php endif; ?>
                    <input type="range" class="price-slider" name="<?= $this->formparam(['f_price', 1]) ?>"
                           min="0" max="<?= $enc->html($this->get('priceHigh', $this->param('f_price/1', 0))) ?>"
                           step="1"
                           value="<?= $enc->html($this->param('f_price/1', $this->get('priceHigh', 0))) ?>">
                </div>
                <button type="submit"
                        class="btn btn-primary"><?= $enc->html($this->translate('client', 'Save')) ?></button>
            </fieldset>
        </div>
    </section>
<?php endif ?>
<?php $this->block()->stop(); ?>
<?= $this->block()->get('catalog/filter/price'); ?>

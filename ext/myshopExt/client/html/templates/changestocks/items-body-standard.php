<?php

$enc = $this->encoder();
$listTarget = $this->config('client/html/changestocks/url/target');
$listController = $this->config('client/html/changestocks/url/controller', 'changestocks');
$listAction = $this->config('client/html/changestocks/url/action', 'list');
$listConfig = $this->config('client/html/changestocks/url/config', []);
$infiniteScroll = $this->config('client/html/changestocks/infinite-scroll', false);


$position = $this->get('itemPosition');
$detailTarget = $this->config('client/html/catalog/detail/url/target');
$detailController = $this->config('client/html/catalog/detail/url/controller', 'catalog');
$detailAction = $this->config('client/html/catalog/detail/url/action', 'detail');
$detailConfig = $this->config('client/html/catalog/detail/url/config', []);
$detailFilter = array_flip($this->config('client/html/catalog/detail/url/filter', ['d_prodid']));

$stockManager = $this->get('stockManager');
$stockManagerSaver = $this->get('stockManagerSaver');

$codes = $this->get("productCodes");

$stocks = $stockManager->search();

if (isset($_POST["save"])) {
    save($this->request()->getParsedBody(), $stockManager, $stocks,$stockManagerSaver );
}

$stockLevel = [];
$deliveryTime = [];
foreach ($stocks as $stockItem) {
    $stockLevel[$stockItem->get('stock.productcode')] = $stockItem->get('stock.stocklevel');
    $deliveryTime[$stockItem->get('stock.productcode')] = $stockItem->get('stock.timeframe');
}

$userName = $this->get('user')->get('customer.code');

?>


<?php $this->block()->start('changestocks/items'); ?>

<form action="" method="POST" name="modify">
    <?= $this->csrf()->formfield(); ?>
    <div class="catalog-list-items"
         data-infinite-url="<?= $infiniteScroll && $this->get('listPageNext', 0) >
         $this->get('listPageCurr', 0) ? $this->url($listTarget, $listController,
             $listAction, array('l_page' => $this->get('listPageNext')) +
             $this->get('listParams', []), [], $listConfig) : '' ?>">

        <h2><?= $enc->html($this->translate('client', 'Your products'), $enc::TRUST); ?></h2>

        <table style="width:100%">
            <tr>
                <th><?= $enc->html($this->translate('client', 'Product'), $enc::TRUST); ?></th>
                <th><?= $enc->html($this->translate('client', 'Price'), $enc::TRUST); ?></th>
                <th><?= $enc->html($this->translate('client', 'Stock'), $enc::TRUST); ?></th>
                <th><?= $enc->html($this->translate('client', 'Delivery'), $enc::TRUST); ?></th>
            </tr>

            <?php foreach ($this->get('listProductItems', []) as $id => $productItem) : ?>
                <?php
                $allowEdit = false;
                $productConfig = $productItem->get('product.config');
                $suppliers = $productItem->get('.supplier');
                if (!($suppliers === null)) {
                    foreach ($suppliers as $supplier) {
                        if ($supplier->get('supplier.code') === $userName) {
                            $allowEdit = true;
                            continue;
                        }
                    }
                }
                foreach ($productConfig as $property => $configProperty):
                    if (substr($property, 0, 7) === "Editor_" && $configProperty === $userName)
                        $allowEdit = true;
                endforeach;

                if (!$allowEdit)
                    continue;
                ?>
                <tr>
                    <th>
                        <?php if (($mediaItem = $productItem->getRefItems('media', 'default', 'default')->first()) !== null) : ?>
                            <img height="70"
                                 class="lazy-image"
                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEEAAEALAAAAAABAAEAAAICTAEAOw=="
                                 data-src="<?= $enc->attr($this->content($mediaItem->getPreview())); ?>"
                                 data-srcset="<?= $enc->attr($this->imageset($mediaItem->getPreviews())) ?>"
                                 alt="<?= $enc->attr($mediaItem->getName()); ?>"
                            />

                        <?php endif; ?>

                        <?= $enc->html($productItem->getName(), $enc::TRUST); ?>

                    </th>
                    <th>
                        <?= $this->partial(
                            $this->config('client/html/common/partials/price', 'common/partials/price-standard'),
                            ['prices' => $productItem->getRefItems('price', null, 'default')]
                        ); ?>
                    </th>
                    <th>
                        <input type="number" step="1"
                               name=<?= $productItem->get('product.code') . "_stockLevel" ?> value= <?= $stockLevel[$productItem->
                        get('product.code')] ?> >
                    </th>
                    <th>
                        <input type="number" step="1"
                               name=<?= $productItem->get('product.code') . "_delivDays" ?>
                               value= <?= $deliveryTime[$productItem->get('product.code')] ?> >
                    </th>

                </tr>
            <?php endforeach; ?>

        </table>
    </div>

    <div align="center">
        <button class="btn btn-primary btn-lg" type="submit" name="save" value="save">
            <?= $enc->html($this->translate('client', 'Save'), $enc::TRUST); ?>
        </button>
    </div>
    <?php $this->block()->stop(); ?>


    <?php

    function save(array $input, $stockManager, $stocks,$stockManagerSaver )
    {
        $putStock = [];
        $putDelivery = [];
        foreach ($input as $id => $value) {
            if (!($id === "_token") && endswith($id, "_stockLevel")) {
                $putStock = putStock(substr($id, 0, -11), $value, $putStock);
            } else if (!($id === "_token") && endswith($id, "_delivDays")) {
                $putDelivery = putDelivery(substr($id, 0, -10), $value, $putDelivery);
            }
        }

        foreach ($stocks as $stockItem) {
            $saveNeeded = false;
            if (array_key_exists($stockItem->get('stock.productcode'), $putStock))
                $newValueStock = $putStock[$stockItem->get('stock.productcode')];
            elseif (substr($stockItem->get('stock.productcode'), -2) === "_S") { ///selectionItem;
                if (!(array_key_exists(substr($stockItem->get('stock.productcode'), 0, -2), $putStock)))
                    continue;
                $codeProd = substr($stockItem->get('stock.productcode'), 0, -2);
                $newValueStock = $putStock[$codeProd];
            } else {
                continue;
            }
            if (!($newValueStock === $stockItem->get('stock.stocklevel'))) {
                $stockItem->setStockLevel($newValueStock);
                $saveNeeded = true;
            }

            if (array_key_exists($stockItem->get('stock.productcode'), $putDelivery)) {
                $newValueDelivery = $putDelivery[$stockItem->get('stock.productcode')];
                if (!($newValueDelivery === $stockItem->get('stock.timeframe'))) {
                    $stockItem->setTimeframe($newValueDelivery);
                    $saveNeeded = true;
                }
            }

            if ($saveNeeded) {
                $stockManagerSaver->saveItem($stockItem);
            }
        }
    }

    function endswith($string, $test)
    {
        $strlen = strlen($string);
        $testlen = strlen($test);
        if ($testlen > $strlen) return false;
        return substr_compare($string, $test, $strlen - $testlen, $testlen) === 0;
    }

    function putStock($id, $value, $putStock): array
    {
        $putStock[$id] = $value;
        return $putStock;
    }

    function putDelivery($id, $value, $putDelivery): array
    {
        $putDelivery[$id] = $value;
        return $putDelivery;
    }


    ?>

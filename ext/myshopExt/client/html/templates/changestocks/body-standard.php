<?php
$enc = $this->encoder();

?>
<section
    class="aimeos">

    <?php if (isset($this->listErrorList)) : ?>
        <ul class="error-list">
            <?php foreach ((array)$this->listErrorList as $errmsg) : ?>
                <li class="error-item"><?= $enc->html($errmsg); ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>


    <?php if (($catItem = $this->get('listCatPath', map())->last()) !== null) : ?>
        <div class="catalog-list-head">
            <h1><?= $enc->html($catItem->getName()); ?></h1>
        </div>
    <?php endif; ?>

    <?php if (($searchText = $this->param('f_search', null)) != null) : ?>
        <div class="list-search">

            <?php if (($total = $this->get('listProductTotal', 0)) > 0) : ?>
                <?= $enc->html(sprintf(
                    $this->translate(
                        'client',
                        'Search result for <span class="searchstring">"%1$s"</span> (%2$d article)',
                        'Search result for <span class="searchstring">"%1$s"</span> (%2$d articles)',
                        $total
                    ),
                    $searchText,
                    $total
                ), $enc::TRUST); ?>
            <?php else : ?>
                <?= $enc->html(sprintf(
                    $this->translate(
                        'client',
                        'No articles found for <span class="searchstring">"%1$s"</span>. Please try again with a different keyword.'
                    ),
                    $searchText
                ), $enc::TRUST); ?>
            <?php endif; ?>

        </div>
    <?php endif; ?>

    <?= $this->block()->get('changestocks/items'); ?>

</section>

<?php
$enc = $this->encoder();
$listTarget = $this->config('client/html/ordertemplates/url/target');
$listController = $this->config('client/html/ordertemplates/url/controller', 'changeprices');
$listAction = $this->config('client/html/ordertemplates/url/action', 'list');
$listConfig = $this->config('client/html/ordertemplates/url/config', []);
$infiniteScroll = $this->config('client/html/ordertemplates/infinite-scroll', false);

if (isset($_POST["apply"])) {
    $idSelected = getSelectedTemplateID($this->request()->getParsedBody());
    $templateSelected = $this->get('orderTemplates')[$idSelected];
    $supplierController = $this->get("supplierController");

    foreach ($templateSelected as $id => $product):
        $suppControllerNew = clone $supplierController;
        $supplierSearch = $suppControllerNew->has('product', 'default',
            $product["product"])->search()->first();

        $supplier = $supplierSearch->get('supplier.code');

        $this->basketController->addProduct(
            $product["product"],
            (float)$product["quantity"], [], [], [], 'default',
            $supplier, '');

    endforeach;
}
function getSelectedTemplateID(array $requestInput)
{

    $id = null;
    foreach ($requestInput as $idEntry => $value): int :
        if ($idEntry === "_token")
            continue;
        if ($idEntry === "apply")
            break;
        $id = $idEntry;
    endforeach;
    return $id;
}

?>
<section
    class="aimeos">

    <?php if (isset($this->listErrorList)) : ?>
        <ul class="error-list">
            <?php foreach ((array)$this->listErrorList as $errmsg) : ?>
                <li class="error-item"><?= $enc->html($errmsg); ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

    <form method="POST">
        <?= $this->csrf()->formfield(); ?>
        <div class="catalog-list-items"
             data-infinite-url="<?= $infiniteScroll && $this->get('listPageNext', 0) >
             $this->get('listPageCurr', 0) ? $this->url($listTarget, $listController,
                 $listAction, array('l_page' => $this->get('listPageNext')) +
                 $this->get('listParams', []), [], $listConfig) : '' ?>">

            <h2><?= $enc->html($this->translate('client', 'Your products'), $enc::TRUST); ?></h2>

            <table style="width:100%">
                <tr>
                    <th><?= $enc->html($this->translate('client', 'TemplateID'), $enc::TRUST); ?></th>
                    <th><?= $enc->html($this->translate('client', 'Change'), $enc::TRUST); ?></th>
                </tr>

                <?php foreach ($this->get('orderTemplates', []) as $id => $orderTemplate) : ?>
                    <tr>
                        <th>
                            <h2><?= $id ?></h2>
                        </th>
                        <th>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target=<?= "#item" . strval($id) ?>>
                                +
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id=<?= "item" . strval($id) ?> aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">
                                                <?= $enc->html($this->translate('client', 'TemplateID'), $enc::TRUST); ?></h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="height:20em;overflow:auto;">
                                            <input type="text"
                                                   id=<?= "inputName" .strval($id) ?>
                                                   onkeyup='myFunction("<?= "inputName" .strval($id) ?>","<?= "tabName" .strval($id) ?>")'
                                                   placeholder=<?= $enc->html($this->translate('client', 'Search'), $enc::TRUST); ?>>
                                            <table style="width:100%" id=<?= "tabName" .strval($id) ?>>
                                                <tr>
                                                    <th><?= $enc->html($this->translate('client', 'Product'), $enc::TRUST); ?></th>
                                                    <th><?= $enc->html($this->translate('client', 'Quantity'), $enc::TRUST); ?></th>
                                                </tr>
                                                <?php foreach ($orderTemplate as $entry) {
                                                    ?>
                                                    <tr>
                                                        <input type="hidden"
                                                               name=<?= strval($id) ?> value=<?= $id ?>>
                                                        <th><?php if (($mediaItem = $entry["product"]->getRefItems('media', 'default', 'default')->first()) !== null) : ?>
                                                                <img height="70"
                                                                     class="lazy-image"
                                                                     src="data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEEAAEALAAAAAABAAEAAAICTAEAOw=="
                                                                     data-src="<?= $enc->attr($this->content($mediaItem->getPreview())); ?>"
                                                                     data-srcset="<?= $enc->attr($this->imageset($mediaItem->getPreviews())) ?>"
                                                                     alt="<?= $enc->attr($mediaItem->getName()); ?>"
                                                                />
                                                            <?php endif; ?><?= $entry["product"]->getName(); ?></th>
                                                        <th><?= $entry["quantity"]; ?>
                                                        </th>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                <?= $enc->html($this->translate('client', 'Close'), $enc::TRUST); ?>
                                            </button>
                                            <button class="btn btn-primary" type="submit" name="apply">
                                                <?= $enc->html($this->translate('client', 'Apply'), $enc::TRUST); ?>
                                            </button>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </th>

                    </tr>
                <?php endforeach; ?>

            </table>
        </div>
    </form>

</section>

<script>
    function myFunction(inputName,tabName) {
        // Declare variables
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById(inputName);
        filter = input.value.toUpperCase();
        table = document.getElementById(tabName);
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 1; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("th")[0];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>

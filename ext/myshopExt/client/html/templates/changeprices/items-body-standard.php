<?php

$enc = $this->encoder();
$listTarget = $this->config('client/html/changeprices/url/target');
$listController = $this->config('client/html/changeprices/url/controller', 'changeprices');
$listAction = $this->config('client/html/changeprices/url/action', 'list');
$listConfig = $this->config('client/html/changeprices/url/config', []);
$infiniteScroll = $this->config('client/html/changeprices/infinite-scroll', false);


$position = $this->get('itemPosition');
$detailTarget = $this->config('client/html/catalog/detail/url/target');
$detailController = $this->config('client/html/catalog/detail/url/controller', 'catalog');
$detailAction = $this->config('client/html/catalog/detail/url/action', 'detail');
$detailConfig = $this->config('client/html/catalog/detail/url/config', []);
$detailFilter = array_flip($this->config('client/html/catalog/detail/url/filter', ['d_prodid']));

$codes = [];
foreach ($this->get('listProductItems') as $productItem)
    array_push($codes, $productItem->get('product.code'));

$allDiscounts = $this->get('productDiscounts');
?>

<?php $this->block()->start('changeprices/items'); ?>

<form action="/changeprices" method="POST">
    <?= $this->csrf()->formfield(); ?>
    <div class="catalog-list-items"
         data-infinite-url="<?= $infiniteScroll && $this->get('listPageNext', 0) >
         $this->get('listPageCurr', 0) ? $this->url($listTarget, $listController,
             $listAction, array('l_page' => $this->get('listPageNext')) +
             $this->get('listParams', []), [], $listConfig) : '' ?>">

        <h2><?= $enc->html($this->translate('client', 'Your products'), $enc::TRUST); ?></h2>

        <table style="width:100%" id="Table" onload="makeTableScroll();">
            <tr>
                <th><?= $enc->html($this->translate('client', 'Product'), $enc::TRUST); ?></th>
                <th><?= $enc->html($this->translate('client', 'Price'), $enc::TRUST); ?></th>
                <th><?= $enc->html($this->translate('client', 'Discounts'), $enc::TRUST); ?></th>
            </tr>

            <?php $index=0; ?>
            <?php foreach ($this->get('listProductItems', []) as $id => $productItem) : ?>
                <?php $index+=1; ?>
                <tr>
                    <th>
                        <?php if (($mediaItem = $productItem->getRefItems('media', 'default', 'default')->first()) !== null) : ?>
                            <img height="70"
                                 class="lazy-image"
                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///////yH5BAEEAAEALAAAAAABAAEAAAICTAEAOw=="
                                 data-src="<?= $enc->attr($this->content($mediaItem->getPreview())); ?>"
                                 data-srcset="<?= $enc->attr($this->imageset($mediaItem->getPreviews())) ?>"
                                 alt="<?= $enc->attr($mediaItem->getName()); ?>"
                            />

                        <?php endif; ?>

                        <?= $enc->html($productItem->getName(), $enc::TRUST); ?>

                    </th>
                    <th>
                        <?= $this->partial(
                            $this->config('client/html/common/partials/price', 'common/partials/price-standard'),
                            ['prices' => $productItem->getRefItems('price', null, 'default')]
                        ); ?>
                    </th>
                    <th>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target=<?= "#discounts_" . $id ?>>
                            +
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id=<?= "discounts_" . $id ?>
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">
                                            <?= $enc->html($this->translate('client', 'Product Discounts'), $enc::TRUST); ?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div style="height:20em;overflow:auto;">
                                            <input type="text"
                                                   id=<?= "inputName" .strval($index) ?>
                                                   onkeyup='myFunction("<?= "inputName" .strval($index) ?>","<?= "tabName" .strval($index) ?>")'
                                                   placeholder=<?= $enc->html($this->translate('client', 'Search'), $enc::TRUST); ?>>
                                        <table style="width:100%" id=<?= "tabName" .strval($index) ?>>
                                            <input type="hidden"
                                                   name=<?= strval($index) . "_product" ?> value=<?= $id ?>>
                                            <tr>
                                                <th><?= $enc->html($this->translate('client', 'Customer'), $enc::TRUST); ?></th>
                                                <th><?= $enc->html($this->translate('client', 'Discount'), $enc::TRUST); ?></th>
                                            </tr>
                                            <?php foreach ($allDiscounts as $discount) {
                                                if (!($discount["product_id"] === $id))
                                                    continue;
                                                ?>
                                                <tr>
                                                    <th><?= $discount["customerName"]; ?></th>
                                                    <th><input type="text"
                                                               name=<?= strval($index)."_customer_".$discount["customer_id"] ?>
                                                               value=<?= $discount["discount"] ?>>%
                                                    </th>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            <?= $enc->html($this->translate('client', 'Close'), $enc::TRUST); ?>
                                        </button>
                                        <button class="btn btn-primary" type="submit" name="save">
                                            <?= $enc->html($this->translate('client', 'Save'), $enc::TRUST); ?>
                                        </button>
                                     </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </th>

                </tr>
            <?php endforeach; ?>

        </table>
    </div>
</form>

<script>
    function myFunction(inputName,tabName) {
        // Declare variables
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById(inputName);
        filter = input.value.toUpperCase();
        table = document.getElementById(tabName);
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 1; i < tr.length; i++) {
            th = tr[i].getElementsByTagName("th")[0];
            if (th) {
                txtValue = th.textContent || th.innerText;

                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>
<?php $this->block()->stop(); ?>

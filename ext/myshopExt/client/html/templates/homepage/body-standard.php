<?php

$enc = $this->encoder();
$pos = 0;
?>


<body>

<div class="hero-wrap">
    <div class="home-slider owl-carousel owl-loaded owl-drag">


        <div class="slider-item" style="background-image:url(files/bg_1.jpg);">
            <div class="overlay"></div>
            <div class="container">
                <div class="row no-gutters slider-text align-items-center justify-content-center">
                    <div class="col-md-12 ftco-animate">
                        <div class="text w-100 text-center">
                            <h2><?= $enc->html($this->translate('client', 'Potential'), $enc::TRUST); ?></h2>
                            <h1 class="mb-3"><?= $enc->html($this->translate('client', 'Potential (text)'), $enc::TRUST); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="slider-item" style="background-image:url(files/bg_2.jpg);">
            <div class="overlay"></div>
            <div class="container">
                <div class="row no-gutters slider-text align-items-center justify-content-center">
                    <div class="col-md-12 ftco-animate">
                        <div class="text w-100 text-center">
                            <h2><?= $enc->html($this->translate('client', 'Quality'), $enc::TRUST); ?></h2>
                            <h1 class="mb-3"><?= $enc->html($this->translate('client', 'Quality (text)'), $enc::TRUST); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="slider-item" style="background-image:url(files//bg_3.jpg);">
            <div class="overlay"></div>
            <div class="container">
                <div class="row no-gutters slider-text align-items-center justify-content-center">
                    <div class="col-md-12 ftco-animate">
                        <div class="text w-100 text-center">
                            <h2><?= $enc->html($this->translate('client', 'Choice'), $enc::TRUST); ?></h2>
                            <h1 class="mb-3"><?= $enc->html($this->translate('client', "Choice (text)"), $enc::TRUST); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section ftco-no-pt ftco-no-pb">
    <div class="container">
        <div class="row">

            <div class="col-md-3 d-flex services align-self-stretch p-4 py-md-5 ftco-animate">
                <div class="media block-6 d-block text-center pt-md-4">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="connect_icon"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading"><?= $enc->html($this->translate('client', "We connect"), $enc::TRUST); ?></h3>
                        <p><?= $enc->html($this->translate('client', "We connect (text)"), $enc::TRUST); ?></p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 d-flex services align-self-stretch p-4 py-md-5 ftco-animate">
                <div class="media block-6 d-block text-center pt-md-4">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="newhorizons_icon"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading"><?= $enc->html($this->translate('client', "New clients"), $enc::TRUST); ?></h3>
                        <p><?= $enc->html($this->translate('client', "New clients (text)"), $enc::TRUST); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex services align-self-stretch p-4 py-md-5 ftco-animate">
                <div class="media block-6 d-block text-center pt-md-4">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="search_icon"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading"><?= $enc->html($this->translate('client', "Search you want"), $enc::TRUST); ?></h3>
                        <p><?= $enc->html($this->translate('client', "Search you want (text)"), $enc::TRUST); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex services align-self-stretch p-4 py-md-5 ftco-animate">
                <div class="media block-6 d-block text-center pt-md-4">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="available_icon"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading"><?= $enc->html($this->translate('client', "Service Quality"), $enc::TRUST); ?></h3>
                        <p><?= $enc->html($this->translate('client', "Service Quality (text)"), $enc::TRUST); ?></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row no-gutters">
            <div class="col-md-12 col-lg-4 services-2 p-4 py-5 d-flex ftco-animate">
                <div class="py-3 d-flex">
                    <div class="icon">
                        <span class="respo_icon"></span>
                    </div>
                    <div class="text">
                        <h3><?= $enc->html($this->translate('client', "Responsability"), $enc::TRUST); ?></h3>
                        <p class="mb-0"><?= $enc->html($this->translate('client', "Responsability (text)"), $enc::TRUST); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-4 services-2 p-4 py-5 d-flex ftco-animate">
                <div class="py-3 d-flex">
                    <div class="icon">
                        <span class="support_icon"></span>
                    </div>
                    <div class="text">
                        <h3><?= $enc->html($this->translate('client', "Support"), $enc::TRUST); ?></h3>
                        <p class="mb-0"><?= $enc->html($this->translate('client', "Support (text)"), $enc::TRUST); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-4 services-2 p-4 py-5 d-flex ftco-animate">
                <div class="py-3 d-flex">
                    <div class="icon">
                        <span class="difference_icon"></span>
                    </div>
                    <div class="text">
                        <h3><?= $enc->html($this->translate('client', "Difference"), $enc::TRUST); ?></h3>
                        <p class="mb-0"><?= $enc->html($this->translate('client', "Difference (text)"), $enc::TRUST); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section ftco-no-pt ftco-no-pb">
    <div class="container">
        <div class="row d-flex no-gutters">
            <div class="col-md-6 d-flex">
                <div
                    class="img img-video d-flex align-self-stretch align-items-center justify-content-center justify-content-md-end"
                    style="background-image:url('/files/about.jpeg');">
                    <a href="/files/Lifts_VA.mp4" class="icon-video d-flex justify-content-center align-items-center">
                        <span class="icon-play"></span>
                    </a>
                </div>
            </div>
            <div class="col-md-6 pl-md-5">
                <div class="row justify-content-start py-5">
                    <div class="col-md-12 heading-section ftco-animate pl-md-4 py-md-4">
                        <span
                            class="subheading"><?= $enc->html($this->translate('client', "Welcome!"), $enc::TRUST); ?></span>
                        <h2 class="mb-4"><?= $enc->html($this->translate('client', "Dreams (text)"), $enc::TRUST); ?></h2>
                        <p>
                            <?= $enc->html($this->translate('client', "Leading platform"), $enc::TRUST); ?>
                        </p>
                        <div class="tabulation-2 mt-4">
                            <ul class="nav nav-pills nav-fill d-md-flex d-block">
                                <li class="nav-item mb-md-0 mb-2">
                                    <a class="nav-link active py-2" data-toggle="tab"
                                       href="#home1"><?= $enc->html($this->translate('client', "Our mission"), $enc::TRUST); ?></a>
                                </li>
                                <li class="nav-item px-lg-2 mb-md-0 mb-2">
                                    <a class="nav-link py-2" data-toggle="tab"
                                       href="#home2"><?= $enc->html($this->translate('client', "Our vision"), $enc::TRUST); ?></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link py-2 mb-md-0 mb-2" data-toggle="tab"
                                       href="#home3"><?= $enc->html($this->translate('client', "Our value"), $enc::TRUST); ?></a>
                                </li>
                            </ul>
                            <div class="tab-content bg-light rounded mt-2">
                                <div class="tab-pane container p-0 active" id="home1">
                                    <p><?= $enc->html(
                                            $this->translate('client', "Our mission (text)"),
                                            $enc::TRUST
                                        ); ?>
                                    </p>
                                </div>
                                <div class="tab-pane container p-0 fade" id="home2">
                                    <p><?= $enc->html(
                                            $this->translate('client', "Our vision (text)"),
                                            $enc::TRUST
                                        ); ?>
                                    </p>
                                </div>
                                <div class="tab-pane container p-0 fade" id="home3">
                                    <p><?= $enc->html(
                                            $this->translate('client', "Our value (text)"),
                                            $enc::TRUST
                                        ); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-counter" id="section-counter">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 d-flex">
                    <div class="text d-flex align-items-center">
                        <strong class="number" data-number="<?= date('Y') - 2020 ?>">0</strong>
                    </div>
                    <div class="text-2">
							<span><?= $enc->html(
                                    (date('Y') - 2020) > 1 ? $this->translate('client', "Experience years") : $this->translate('client', "Experience year"),
                                    $enc::TRUST
                                ); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 d-flex">
                    <div class="text d-flex align-items-center">
                        <strong class="number" data-number="<?= $this->get("productsCount") ?>">0</strong>
                    </div>
                    <div class="text-2">
							<span><?= $enc->html(
                                    $this->translate('client', "Number of products"),
                                    $enc::TRUST
                                ); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 d-flex">
                    <div class="text d-flex align-items-center">
                        <strong class="number" data-number="<?= $this->get("suppliersCount") ?>">0</strong>
                    </div>
                    <div class="text-2">
							<span><?= $enc->html(
                                    $this->translate('client', "Number of suppliers"),
                                    $enc::TRUST
                                ); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 d-flex">
                    <div class="text d-flex align-items-center">
                        <strong class="number" data-number="<?= $this->get("customersCount") ?>">0</strong>
                    </div>
                    <div class="text-2">
							<span><?= $enc->html(
                                    $this->translate('client', "Number of clients"),
                                    $enc::TRUST
                                ); ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

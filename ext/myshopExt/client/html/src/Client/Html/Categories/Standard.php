<?php

namespace Aimeos\Client\Html\Categories;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Standard
    extends \Aimeos\Client\Html\Common\Client\Factory\Base
    implements \Aimeos\Client\Html\Common\Client\Factory\Iface
{
    private $subPartPath = 'client/html/categories/standard/subparts';
    private $subPartNames = [];

    public function getBody(string $uid = ''): string
    {
        $view = $this->getView();

        $html = '';
        foreach ($this->getSubClients() as $subclient)
            $html .= $subclient->setView($view)->getBody($uid);

        $view->itemsBody = $html;
        $default = 'categories/body-standard';

        $this->addData($view);
        return $view->render($default);
    }

    public function getHeader(string $uid = ''): ?string
    {
        $view = $this->getView();

        $html = '';
        foreach ($this->getSubClients() as $subclient) {
            $html .= $subclient->setView($view)->getHeader($uid);
        }
        $view->itemsHeader = $html;
        $default = 'categories/header-standard';

        return $view->render($default);
    }

    public function getSubClient(string $type, string $name = null): \Aimeos\Client\Html\Iface
    {
        return $this->createSubClient('categories/' . $type, $name);
    }

    protected function getSubClientNames(): array
    {
        return $this->getContext()->getConfig()->get($this->subPartPath, $this->subPartNames);
    }

    public function addData(\Aimeos\MW\View\Iface $view, array &$tags = [], string &$expire = null): \Aimeos\MW\View\Iface
    {

        $cntl = \Aimeos\Controller\Frontend::create($this->getContext(), 'catalog')
            ->uses(['text', 'media'])->root();

        $level = $cntl::LIST;
        $view->treeCatalogTree = $cntl->getTree($level);

        return parent::addData($view, $tags, $expire);
    }

}

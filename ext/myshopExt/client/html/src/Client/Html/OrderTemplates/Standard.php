<?php

namespace Aimeos\Client\Html\OrderTemplates;

use Aimeos\Client\Html\Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Standard
    extends \Aimeos\Client\Html\Common\Client\Factory\Base
    implements \Aimeos\Client\Html\Common\Client\Factory\Iface
{
    private $subPartPath = 'client/html/ordertemplates/standard/subparts';
    private $subPartNames = [];

    public function getBody(string $uid = ''): string
    {
        $view = $this->getView();

        $html = '';
        foreach ($this->getSubClients() as $subclient) {
            $html .= $subclient->setView($view)->getBody($uid);
        }
        $view->itemsBody = $html;
        $tplconf = 'client/html/ordertemplates/standard/template-body';
        $default = 'ordertemplates/body-standard';

        $this->addData($view);
        return $view->render($this->getTemplatePath($tplconf, $default));
    }

    public function getHeader(string $uid = ''): ?string
    {
        $view = $this->getView();

        $html = '';
        foreach ($this->getSubClients() as $subclient) {
            $html .= $subclient->setView($view)->getHeader($uid);
        }
        $view->itemsHeader = $html;
        $tplconf = 'client/html/ordertemplates/standard/template-header';
        $default = 'ordertemplates/header-standard';

        return $view->render($this->getTemplatePath($tplconf, $default));
    }

    public function getSubClient(string $type, string $name = null): \Aimeos\Client\Html\Iface
    {
        return $this->createSubClient('ordertemplates/' . $type, $name);
    }

    protected function getSubClientNames(): array
    {
        return $this->getContext()->getConfig()->get($this->subPartPath, $this->subPartNames);
    }

    public function modifyBody(string $content, string $uid): string
    {
        $content = parent::modifyBody($content, $uid);

        return $this->replaceSection($content, $this->getView()->csrf()->formfield(), 'ordertemplates.csrf');
    }

    public function addData(\Aimeos\MW\View\Iface $view, array &$tags = [], string &$expire = null): \Aimeos\MW\View\Iface
    {
        if (!(Auth::user() === null)):

            $userID = Auth::user()->id;

            $orderTemplatesDB = DB::table("order_templates")->join("order_templates_content", "order_templates.id", "=",
                "order_templates_content.id")->select("order_templates.id",
                "order_templates_content.product_id", "order_templates_content.quantity")->
            where("order_templates.customer_id", "=", $userID)->get()->sortBy("id")->all();
            $productIDsUsed = [];
            foreach ($orderTemplatesDB as $entry)
                array_push($productIDsUsed, $entry->product_id);

            $productIDsUsed = array_unique($productIDsUsed);
            $orderTemplates = [];
            $orderTemplate = [];
            $context = $this->getContext();
            $productManager = \Aimeos\Controller\Frontend::create($context,
                'product')->uses(['attribute', 'media', 'price', 'product', 'text']);
            $productsAll = $productManager->search()->toArray();

            $currentID = null;
            foreach ($orderTemplatesDB as $orderItem):
                if ($currentID === null)
                    $currentID = $orderItem->id;

                if (!($currentID === $orderItem->id)) {
                    $orderTemplates[$currentID] = $orderTemplate;
                    $orderTemplate = [];
                    $currentID = $orderItem->id;
                }

                if (array_key_exists($orderItem->product_id, $productsAll)):
                    $orderTemplateProduct = [];

                    $orderTemplateProduct["product"] = $productsAll[$orderItem->product_id];
                    $orderTemplateProduct["quantity"] = $orderItem->quantity;

                    $orderTemplate[$orderItem->product_id] = $orderTemplateProduct;
                endif;
            endforeach;
            $orderTemplates[$currentID] = $orderTemplate;
            $view->orderTemplates = $orderTemplates;
            $view->basketController = \Aimeos\Controller\Frontend::create($context, 'basket');
            $view->supplierController = \Aimeos\Controller\Frontend::create($context, 'supplier');
        endif;

        return parent::addData($view, $tags, $expire);
    }

}

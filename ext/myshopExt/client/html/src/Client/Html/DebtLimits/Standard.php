<?php

namespace Aimeos\Client\Html\DebtLimits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Standard
    extends \Aimeos\Client\Html\Common\Client\Factory\Base
    implements \Aimeos\Client\Html\Common\Client\Factory\Iface
{
    private $subPartPath = 'client/html/debtLimits/standard/subparts';
    private $subPartNames = [];

    public function getBody(string $uid = ''): string
    {
        $view = $this->getView();

        $html = '';
        foreach ($this->getSubClients() as $subclient) {
            $html .= $subclient->setView($view)->getBody($uid);
        }
        $view->itemsBody = $html;
        $tplconf = 'client/html/debtLimits/standard/template-body';
        $default = 'debtLimits/body-standard';

        $this->addData($view);
        return $view->render($this->getTemplatePath($tplconf, $default));
    }

    public function getHeader(string $uid = ''): ?string
    {
        $view = $this->getView();

        $html = '';
        foreach ($this->getSubClients() as $subclient) {
            $html .= $subclient->setView($view)->getHeader($uid);
        }
        $view->itemsHeader = $html;
        $tplconf = 'client/html/debtLimits/standard/template-header';
        $default = 'debtLimits/header-standard';

        return $view->render($this->getTemplatePath($tplconf, $default));
    }

    public function getSubClient(string $type, string $name = null): \Aimeos\Client\Html\Iface
    {
        return $this->createSubClient('debtLimits/' . $type, $name);
    }

    protected function getSubClientNames(): array
    {
        return $this->getContext()->getConfig()->get($this->subPartPath, $this->subPartNames);
    }

    public function modifyBody(string $content, string $uid): string
    {
        $content = parent::modifyBody($content, $uid);

        return $this->replaceSection($content, $this->getView()->csrf()->formfield(), 'debtLimits.csrf');
    }

    public function addData(\Aimeos\MW\View\Iface $view, array &$tags = [], string &$expire = null): \Aimeos\MW\View\Iface
    {
        if (!(Auth::user() === null)):

            $userID = Auth::user()->id;

            $debtLimitsDB = DB::table("users_list")->join('users', 'users.id', '=', 'users_list.parentid')
                ->leftJoin('debt_limits', function ($join) use ($userID) {
                    $join->on('debt_limits.customer_id', '=', 'users.id')->where('debt_limits.supplier_id', '=', $userID);
                })->leftJoin('debts', function ($join) use ($userID) {
                    $join->on('debts.customer_id', '=', 'users.id')->where('debts.supplier_id', '=', $userID);
                })->
                select('users.id', 'users.name', 'debt_limits.limit','debts.debt')->
                where('users_list.refid', "=", '3')->get()->sortBy("name")->all();;

            $debtLimits = [];
            foreach ($debtLimitsDB as $debtLimit):
                $debtLimits[$debtLimit->id] = $debtLimit;
            endforeach;

            $view->debtLimits = $debtLimits;
        endif;

        return parent::addData($view, $tags, $expire);
    }

}

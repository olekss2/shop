<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Aimeos (aimeos.org), 2018-2020
 * @package Client
 * @subpackage Html
 */


namespace Aimeos\Client\Html\ChangeStocks\Tree;


/**
 * Default implementation of catalog tree HTML client
 *
 * @package Client
 * @subpackage Html
 */
class Standard
    extends \Aimeos\Client\Html\Catalog\Filter\Standard
    implements \Aimeos\Client\Html\Common\Client\Factory\Iface
{
    private $subPartPath = 'client/html/changestocks/tree/standard/subparts';
    private $subPartNames = ['tree'];

    public function getSubClient(string $type, string $name = null): \Aimeos\Client\Html\Iface
    {
        return parent::getSubClient($type, $name);
    }


    /**
     * Returns the names of the subpart clients
     *
     * @return array List of client names
     */
    protected function getSubClientNames(): array
    {
        return $this->getContext()->getConfig()->get($this->subPartPath, $this->subPartNames);
    }
}

<?php

namespace Aimeos\Client\Html\ChangeStocks\Items;

use Aimeos\Client\Html\Exception;

class Standard
    extends \Aimeos\Client\Html\Catalog\Base
    implements \Aimeos\Client\Html\Common\Client\Factory\Iface
{
    private $subPartPath = 'client/html/changestocks/items/standard/subparts';
    private $subPartNames = [];

    public function getBody(string $uid = ''): string
    {
        $view = $this->getView();

        $html = '';
        foreach ($this->getSubClients() as $subclient) {
            $html .= $subclient->setView($view)->getBody($uid);
        }
        $view->itemsBody = $html;
        $tplconf = 'client/html/changestocks/items/standard/template-body';
        $default = 'changestocks/items-body-standard';

        return $view->render($this->getTemplatePath($tplconf, $default));
    }

    public function getHeader(string $uid = ''): ?string
    {
        $view = $this->getView();

        $html = '';
        foreach ($this->getSubClients() as $subclient) {
            $html .= $subclient->setView($view)->getHeader($uid);
        }
        $view->itemsHeader = $html;
        $tplconf = 'client/html/changestocks/items/standard/template-header';
        $default = 'changestocks/items-header-standard';

        return $view->render($this->getTemplatePath($tplconf, $default));
    }

    public function getSubClient(string $type, string $name = null): \Aimeos\Client\Html\Iface
    {
        return $this->createSubClient('changestocks/items/' . $type, $name);
    }

    protected function getSubClientNames(): array
    {
        return $this->getContext()->getConfig()->get($this->subPartPath, $this->subPartNames);
    }

    public function modifyBody(string $content, string $uid): string
    {
        $content = parent::modifyBody($content, $uid);

        return $this->replaceSection($content, $this->getView()->csrf()->formfield(), 'changestocks.items.csrf');
    }

    public function addData(\Aimeos\MW\View\Iface $view, array &$tags = [], string &$expire = null): \Aimeos\MW\View\Iface
    {
        $productItems = map();
        $context = $this->getContext();
        $config = $context->getConfig();
        $products = $view->get('listProductItems', []);


        if ($config->get('client/html/changestocks/basket-add', false)) {
            foreach ($products as $product) {
                if ($product->getType() === 'select') {
                    $productItems->union($product->getRefItems('product', 'default', 'default'));
                }
            }

            $this->addMetaItems($productItems->toArray(), $expire, $tags);

            $view->itemsProductItems = $productItems;
        }
        if (!$products->isEmpty() && (bool)$config->get('client/html/changestocks/stock/enable', true) === true) {
            $view->itemsStockUrl = $this->getStockUrl($view, $products->copy()->union($productItems));
        }

        if (in_array('navigator', $config->get('client/html/changestocks/stage/standard/subparts', ['navigator']))) {
            $size = $config->get('client/html/changestocks/size', 48);
            $size = min(max($view->param('l_size', $size), 1), 100);
            $page = min(max($view->param('l_page', 1), 1), 100);

            $view->itemPosition = ($page - 1) * $size;
        }

        $codes = [];
        foreach ($products as $productItem)
            array_push($codes, $productItem->get('product.code'));

        $view->stockManager = \Aimeos\Controller\Frontend::create($context, 'stock')->code($codes);
        $view->stockManagerSaver = \Aimeos\MShop::create($context, 'stock');
        $view->user = \Aimeos\Controller\Frontend::create( $context, 'customer' )->get();
        $view->productCodes = $codes;
        return parent::addData($view, $tags, $expire);
    }

}

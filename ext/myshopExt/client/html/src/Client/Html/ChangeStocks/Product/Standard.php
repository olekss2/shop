<?php

namespace Aimeos\Client\Html\ChangeStocks\Product;

class Standard
    extends \Aimeos\Client\Html\Catalog\Base
    implements \Aimeos\Client\Html\Common\Client\Factory\Iface
{
    private $subPartPath = 'client/html/changestocks/product/standard/subparts';
    private $subPartNames = [];

    private $tags = [];
    private $expire;
    private $view;

    public function getBody(string $uid = ''): string
    {
        $context = $this->getContext();
        $confkey = 'client/html/changestocks/product';

        if (($html = $this->getCached('body', $uid, [], $confkey)) === null) {
            $view = $this->getView();
            $config = $this->getContext()->getConfig();
            $tplconf = 'client/html/changestocks/product/standard/template-body';
            $default = 'changestocks/product/body-standard';

            try {
                $html = '';

                if (!isset($this->view)) {
                    $view = $this->view = $this->getObject()->addData($view, $this->tags, $this->expire);
                }

                foreach ($this->getSubClients() as $subclient) {
                    $html .= $subclient->setView($view)->getBody($uid);
                }
                $view->listBody = $html;

                $html = $view->render($config->get($tplconf, $default));
                $this->setCached('body', $uid, [], $confkey, $html, $this->tags, $this->expire);

                return $html;
            } catch (\Aimeos\Client\Html\Exception $e) {
                $error = array($context->getI18n()->dt('client', $e->getMessage()));
                $view->productErrorList = array_merge($view->get('productErrorList', []), $error);
            } catch (\Aimeos\Controller\Frontend\Exception $e) {
                $error = array($context->getI18n()->dt('controller/frontend', $e->getMessage()));
                $view->productErrorList = array_merge($view->get('productErrorList', []), $error);
            } catch (\Aimeos\MShop\Exception $e) {
                $error = array($context->getI18n()->dt('mshop', $e->getMessage()));
                $view->productErrorList = array_merge($view->get('productErrorList', []), $error);
            } catch (\Exception $e) {
                $error = array($context->getI18n()->dt('client', 'A non-recoverable error occured'));
                $view->productErrorList = array_merge($view->get('productErrorList', []), $error);
                $this->logException($e);
            }

            $html = $view->render($config->get($tplconf, $default));
        } else {
            $html = $this->modifyBody($html, $uid);
        }

        return $html;
    }

    public function getHeader(string $uid = ''): ?string
    {
        $confkey = 'client/html/changestocks/product';

        if (($html = $this->getCached('header', $uid, [], $confkey)) === null) {
            $view = $this->getView();
            $config = $this->getContext()->getConfig();
            $tplconf = 'client/html/changestocks/product/standard/template-header';
            $default = 'changestocks/product/header-standard';

            try {
                $html = '';

                if (!isset($this->view)) {
                    $view = $this->view = $this->getObject()->addData($view, $this->tags, $this->expire);
                }

                foreach ($this->getSubClients() as $subclient) {
                    $html .= $subclient->setView($view)->getHeader($uid);
                }
                $view->listHeader = $html;

                $html = $view->render($config->get($tplconf, $default));
                $this->setCached('header', $uid, [], $confkey, $html, $this->tags, $this->expire);

                return $html;
            } catch (\Exception $e) {
                $this->logException($e);
            }
        } else {
            $html = $this->modifyHeader($html, $uid);
        }

        return $html;
    }

    public function getSubClient(string $type, string $name = null): \Aimeos\Client\Html\Iface
    {
        return $this->createSubClient('catalog/product/' . $type, $name);
    }

    public function process()
    {
        $context = $this->getContext();
        $view = $this->getView();

        try {
            parent::process();
        } catch (\Aimeos\Client\Html\Exception $e) {
            $error = array($context->getI18n()->dt('client', $e->getMessage()));
            $view->productErrorList = array_merge($view->get('productErrorList', []), $error);
        } catch (\Aimeos\Controller\Frontend\Exception $e) {
            $error = array($context->getI18n()->dt('controller/frontend', $e->getMessage()));
            $view->productErrorList = array_merge($view->get('productErrorList', []), $error);
        } catch (\Aimeos\MShop\Exception $e) {
            $error = array($context->getI18n()->dt('mshop', $e->getMessage()));
            $view->productErrorList = array_merge($view->get('productErrorList', []), $error);
        } catch (\Exception $e) {
            $error = array($context->getI18n()->dt('client', 'A non-recoverable error occured'));
            $view->productErrorList = array_merge($view->get('productErrorList', []), $error);
            $this->logException($e);
        }
    }

    protected function getSubClientNames(): array
    {
        return $this->getContext()->getConfig()->get($this->subPartPath, $this->subPartNames);
    }

    public function modifyBody(string $content, string $uid): string
    {
        $content = parent::modifyBody($content, $uid);

        return $this->replaceSection($content, $this->getView()->csrf()->formfield(), 'changestocks.items.csrf');
    }

    public function addData(\Aimeos\MW\View\Iface $view, array &$tags = [], string &$expire = null): \Aimeos\MW\View\Iface
    {
        $context = $this->getContext();
        $config = $context->getConfig();

        $productItems = map();
        $domains = $config->get('client/html/changestocks/domains', ['media', 'price', 'text']);
        $domains = $config->get('client/html/changestocks/product/domains', $domains);

        if ($config->get('client/html/changestocks/product/basket-add', false)) {
            $domains = array_merge_recursive($domains, ['product' => ['default'], 'attribute']);
        }

        $productCodes = $config->get('client/html/changestocks/product/product-codes', []);

        $products = \Aimeos\Controller\Frontend::create($context, 'product')
            ->compare('==', 'product.code', $productCodes)
            ->slice(0, count($productCodes))
            ->uses($domains)
            ->search();

        // Sort products by the order given in the configuration "client/html/catalog/product/product-codes".
        $productCodesOrder = array_flip($productCodes);
        $products->usort(function ($a, $b) use ($productCodesOrder) {
            return $productCodesOrder[$a->getCode()] - $productCodesOrder[$b->getCode()];
        });

        if ($config->get('client/html/changestocks/product/basket-add', false)) {
            foreach ($products as $product) {
                if ($product->getType() === 'select') {
                    $productItems->union($product->getRefItems('product', 'default', 'default'));
                }
            }
        }
        if (!$products->isEmpty() && (bool)$config->get('client/html/catalog/changestocks/stock/enable', true) === true) {
            $view->itemsStockUrl = $this->getStockUrl($view, $products->union($productItems));
        }

        // Delete cache when products are added or deleted even when in "tag-all" mode
        $this->addMetaItems($products->union($productItems), $expire, $tags, ['product']);

        $view->productItems = $products;
        $view->productTotal = count($products);
        $view->productProductItems = $productItems;

        return parent::addData($view, $tags, $expire);
    }
}

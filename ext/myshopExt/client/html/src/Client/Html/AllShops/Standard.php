<?php

namespace Aimeos\Client\Html\AllShops;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Standard
    extends \Aimeos\Client\Html\Common\Client\Factory\Base
    implements \Aimeos\Client\Html\Common\Client\Factory\Iface
{
    private $subPartPath = 'client/html/allShops/standard/subparts';
    private $subPartNames = [];

    public function getBody(string $uid = ''): string
    {
        $view = $this->getView();

        $html = '';
        foreach ($this->getSubClients() as $subclient)
            $html .= $subclient->setView($view)->getBody($uid);

        $view->itemsBody = $html;
        $default = 'allShops/body-standard';

        $this->addData($view);
        return $view->render($default);
    }

    public function getHeader(string $uid = ''): ?string
    {
        $view = $this->getView();

        $html = '';
        foreach ($this->getSubClients() as $subclient) {
            $html .= $subclient->setView($view)->getHeader($uid);
        }
        $view->itemsHeader = $html;
        $default = 'allShops/header-standard';

        return $view->render($default);
    }

    public function getSubClient(string $type, string $name = null): \Aimeos\Client\Html\Iface
    {
        return $this->createSubClient('allShops/' . $type, $name);
    }

    protected function getSubClientNames(): array
    {
        return $this->getContext()->getConfig()->get($this->subPartPath, $this->subPartNames);
    }

    public function addData(\Aimeos\MW\View\Iface $view, array &$tags = [], string &$expire = null): \Aimeos\MW\View\Iface
    {
        $context = $this->getContext();

        $view->suppliers = \Aimeos\Controller\Frontend::create($context, 'supplier')->uses(['media', 'address', 'text', 'product'])->search();

        $supplierMinFreeDelivery = [];
        $currencySup = [];
        foreach ($view->suppliers as $supplier):
            $supCode = $supplier->get("supplier.code");
            try {
                $supplierDeliveryPrices = \Aimeos\Controller\Frontend::create($context, 'service')->uses(['price'])->type('delivery')->find($supCode)->getListItems('price');
            } catch (\Aimeos\MShop\Exception $e) {
                $supplierMinFreeDelivery[$supCode] = "-";
                continue;
            }
            $maxVal = -1;
            $currency = "EUR";
            foreach ($supplierDeliveryPrices as $delPrice):
                if ($delPrice->get("price.costs") === 0) {
                    if ($maxVal == -1)
                        $maxVal = 0;
                    break;
                }
                $currency = $delPrice->getRefItem()->get("price.currencyid");
                $priceConfig = $delPrice->get("service.lists.config");
                if (!array_key_exists('MaxValue', $priceConfig))
                    break;

                $maxVal = $priceConfig['MaxValue'];
            endforeach;
            if ($maxVal == -1)
                $supplierMinFreeDelivery[$supCode] = "-";
            else
                $supplierMinFreeDelivery[$supCode] = $maxVal;

            $currencySup[$supCode] = $currency;
        endforeach;

        $view->supplierMinFreeDelivery = $supplierMinFreeDelivery;
        $view->currencySup = $currencySup;

        ////Categories and products
        $supplierProductCount = [];
        $langID = $view->param("locale");
        $supplierCategories = [];
        $productController = \Aimeos\Controller\Frontend::create($context, 'product')->uses(['catalog']);
        foreach ($view->suppliers as $supplier):
            $products = $supplier->getListItems('product');
            $supplierProductCount[$supplier->getCode()] = count($products);
            $categories = [];
            foreach ($products as $productSup):
                if ($productSup->getRefItem() == null)
                    continue;

                $productObject = \Aimeos\Controller\Frontend::create($context, 'product')->uses(['catalog'])->get($productSup->getRefItem()->
                get("product.id"));
                if ($productObject == null)
                    continue;

                $productCatalog = $productObject->get(".catalog");
                if ($productCatalog == null)
                    continue;

                foreach ($productCatalog as $id => $categoryDummy):
                    if ($id == 1 || array_key_exists($id, $categories))
                        continue;
                    $catalogText = \Aimeos\Controller\Frontend::create($context, 'catalog')->uses(['text'])->get($id)->getListItems("text");
                    foreach ($catalogText as $categoryText):
                        $categoryTextRefArr = $catalogText->getRefItem();
                        foreach ($categoryTextRefArr as $categoryTextRef):
                            if ($langID !== $categoryTextRef->get("text.languageid") ||
                                $categoryTextRef->get("text.type") !== "name")
                                continue;

                            $categories[$id] = $categoryTextRef->get("text.content");
                            break;
                        endforeach;
                    endforeach;

                endforeach;
            endforeach;

            $supplierCategories[$supplier->getCode()] = $categories;
        endforeach;

        $view->supplierCategories = $supplierCategories;
        $view->supplierProductCount = $supplierProductCount;
        return parent::addData($view, $tags, $expire);
    }

}

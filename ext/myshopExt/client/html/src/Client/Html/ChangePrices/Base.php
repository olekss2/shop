<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Metaways Infosystems GmbH, 2014
 * @copyright Aimeos (aimeos.org), 2015-2020
 * @package Client
 * @subpackage Html
 */


namespace Aimeos\Client\Html\ChangePrices;


/**
 * Common methods for the catalog HTML client classes.
 *
 * @package Client
 * @subpackage Html
 */
abstract class Base
	extends \Aimeos\Client\Html\Common\Client\Factory\Base
{
	/**
	 * Returns the URL for retrieving the stock levels
	 *
	 * @param \Aimeos\MW\View\Iface $view View instance with helper
	 * @param \Aimeos\MShop\Product\Item\Iface[] List of products with their IDs as keys
	 * @return \Aimeos\Map URLs to retrieve the stock levels for the given products
	 */
	protected function getStockUrl( \Aimeos\MW\View\Iface $view, \Aimeos\Map $products ) : \Aimeos\Map
	{

		$target = $view->config( 'client/html/changeprices/stock/url/target' );
		$cntl = $view->config( 'client/html/changeprices/stock/url/controller', 'changeprices' );

		$action = $view->config( 'client/html/changeprices/stock/url/action', 'stock' );
		$config = $view->config( 'client/html/changeprices/stock/url/config', [] );

		$max = $view->config( 'client/html/changeprices/stock/url/max-items', 100 );


		$urls = [];
		$codes = $products->getCode()->sort();

		while( !( $list = $codes->splice( -$max ) )->isEmpty() ) {
			$urls[] = $view->url( $target, $cntl, $action, array( "s_prodcode" => $list->toArray() ), [], $config );
		}

		return map( $urls )->reverse();
	}
}

<?php

/**
 * @license LGPLv3, http://opensource.org/licenses/LGPL-3.0
 * @copyright Metaways Infosystems GmbH, 2012
 * @copyright Aimeos (aimeos.org), 2015-2020
 * @package Client
 * @subpackage Html
 */


namespace Aimeos\Client\Html\ChangePrices;

class Standard
    extends \Aimeos\Client\Html\ChangePrices\Base
    implements \Aimeos\Client\Html\Common\Client\Factory\Iface
{

    private $subPartPath = 'client/html/changeprices/standard/subparts';
    private $subPartNames = array('items');

    private $tags = [];
    private $expire;
    private $view;

    public function getBody(string $uid = ''): string
    {
        $view = $this->getView();
        $context = $this->getContext();
        $prefixes = ['f_catid', 'f_supid', 'f_sort', 'l_page', 'l_type'];
        $confkey = 'client/html/changeprices';

        $args = map($view->param())->except($prefixes)->filter(function ($val, $key) {
            return !strncmp($key, 'f_', 2) || !strncmp($key, 'l_', 2);
        });

        if (!$args->isEmpty() || ($html = $this->getCached('body', $uid, $prefixes, $confkey)) === null) {
            $tplconf = 'client/html/changeprices/standard/template-body';
            $default = 'changeprices/body-standard';

            try {
                $html = '';

                if (!isset($this->view)) {
                    $view = $this->view = $this->getObject()->addData($view, $this->tags, $this->expire);
                }

                foreach ($this->getSubClients() as $subclient) {
                    $html .= $subclient->setView($view)->getBody($uid);
                }
                $view->listBody = $html;

                $html = $view->render($this->getTemplatePath($tplconf, $default));

                if ($args->isEmpty()) {
                    $this->setCached('body', $uid, $prefixes, $confkey, $html, $this->tags, $this->expire);
                }

                return $html;
            } catch (\Aimeos\Client\Html\Exception $e) {
                $error = array($context->getI18n()->dt('client', $e->getMessage()));
                $view->listErrorList = array_merge($view->get('listErrorList', []), $error);
            } catch (\Aimeos\Controller\Frontend\Exception $e) {
                $error = array($context->getI18n()->dt('controller/frontend', $e->getMessage()));
                $view->listErrorList = array_merge($view->get('listErrorList', []), $error);
            } catch (\Aimeos\MShop\Exception $e) {
                $error = array($context->getI18n()->dt('mshop', $e->getMessage()));
                $view->listErrorList = array_merge($view->get('listErrorList', []), $error);
            } catch (\Exception $e) {
                $error = array($context->getI18n()->dt('client', 'A non-recoverable error occured'));
                $view->listErrorList = array_merge($view->get('listErrorList', []), $error);
                $this->logException($e);
            }

            $html = $view->render($this->getTemplatePath($tplconf, $default));
        } else {
            $html = $this->modifyBody($html, $uid);
        }

        return $html;
    }

    public function getHeader(string $uid = ''): ?string
    {
        $view = $this->getView();
        $confkey = 'client/html/changeprices';
        $prefixes = ['f_catid', 'f_supid', 'f_sort', 'l_page', 'l_type'];

        $args = map($view->param())->except($prefixes)->filter(function ($val, $key) {
            return !strncmp($key, 'f_', 2) || !strncmp($key, 'l_', 2);
        });

        if (!$args->isEmpty() || ($html = $this->getCached('header', $uid, $prefixes, $confkey)) === null) {

            $tplconf = 'client/html/changeprices/standard/template-header';
            $default = 'changeprices/header-standard';

            try {
                $html = '';

                if (!isset($this->view)) {
                    $view = $this->view = $this->getObject()->addData($view, $this->tags, $this->expire);
                }

                foreach ($this->getSubClients() as $subclient) {
                    $html .= $subclient->setView($view)->getHeader($uid);
                }
                $view->listHeader = $html;

                $html = $view->render($this->getTemplatePath($tplconf, $default));

                if ($args->isEmpty()) {
                    $this->setCached('header', $uid, $prefixes, $confkey, $html, $this->tags, $this->expire);
                }

                return $html;
            } catch (\Exception $e) {
                $this->logException($e);
            }
        } else {
            $html = $this->modifyHeader($html, $uid);
        }

        return $html;
    }

    public function getSubClient(string $type, string $name = null): \Aimeos\Client\Html\Iface
    {
        return $this->createSubClient('changeprices/' . $type, $name);
    }

    public function process()
    {
        $context = $this->getContext();
        $view = $this->getView();

        try {
            $site = $context->getLocale()->getSiteItem()->getCode();
            $params = $this->getClientParams($view->param());

            $catId = $context->getConfig()->get('client/html/changeprices/catid-default');

            if (($catId = $view->param('f_catid', $catId))) {
                $params['f_name'] = $view->param('f_name');
                $params['f_catid'] = $catId;
            }

            $context->getSession()->set('aimeos/changeprices/params/last/' . $site, $params);

            parent::process();
        } catch (\Aimeos\Client\Html\Exception $e) {
            $error = array($context->getI18n()->dt('client', $e->getMessage()));
            $view->listErrorList = array_merge($view->get('listErrorList', []), $error);
        } catch (\Aimeos\Controller\Frontend\Exception $e) {
            $error = array($context->getI18n()->dt('controller/frontend', $e->getMessage()));
            $view->listErrorList = array_merge($view->get('listErrorList', []), $error);
        } catch (\Aimeos\MShop\Exception $e) {
            $error = array($context->getI18n()->dt('mshop', $e->getMessage()));
            $view->listErrorList = array_merge($view->get('listErrorList', []), $error);
        } catch (\Exception $e) {
            $error = array($context->getI18n()->dt('client', 'A non-recoverable error occured'));
            $view->listErrorList = array_merge($view->get('listErrorList', []), $error);
            $this->logException($e);
        }
    }

    protected function getSubClientNames(): array
    {
        return $this->getContext()->getConfig()->get($this->subPartPath, $this->subPartNames);
    }

    public function addData(\Aimeos\MW\View\Iface $view, array &$tags = [], string &$expire = null): \Aimeos\MW\View\Iface
    {
        $total = 0;
        $context = $this->getContext();
        $config = $context->getConfig();

        $domains = $config->get('client/html/changeprices/domains', ['media', 'price', 'text','supplier']);
        $domains = $config->get('client/html/changeprices/domains', $domains);

        if ($view->config('client/html/changeprices/basket-add', false)) {
            $domains = array_merge_recursive($domains, ['product' => ['default'], 'attribute']);
        }

        $pages = $config->get('client/html/changeprices/pages', 100);

        $size = $config->get('client/html/changeprices/size', 48);

        $level = $config->get('client/html/changeprices/levels', \Aimeos\MW\Tree\Manager\Base::LEVEL_ONE);

        $catids = $view->param('f_catid', $config->get('client/html/changeprices/catid-default'));
        $catids = $catids != null && is_scalar($catids) ? explode(',', $catids) : $catids; // workaround for TYPO3

        $sort = $view->param('f_sort', $config->get('client/html/changeprices/sort', 'relevance'));
        $size = min(max($view->param('l_size', $size), 1), 100);
        $page = min(max($view->param('l_page', 1), 1), $pages);

        $products = \Aimeos\Controller\Frontend::create($context, 'product')
            ->sort($sort) // prioritize user sorting over the sorting through relevance and category
            ->text($view->param('f_search'))
            ->price($view->param('f_price'))
            ->category($catids, 'default', $level)
            ->supplier($view->param('f_supid', []))
            ->allOf($view->param('f_attrid', []))
            ->oneOf($view->param('f_optid', []))
            ->oneOf($view->param('f_oneid', []))
            ->slice(($page - 1) * $size, $size)
            ->uses($domains)
            ->search($total);

        if ($catids != null) {
            $controller = \Aimeos\Controller\Frontend::create($context, 'changeprices')->uses($domains);
            $listCatPath = $controller->getPath(is_array($catids) ? reset($catids) : $catids);

            if (($categoryItem = $listCatPath->last()) !== null) {
                $view->listCurrentCatItem = $categoryItem;
            }

            $view->listCatPath = $listCatPath;
            $this->addMetaItems($listCatPath, $expire, $tags);
        }


        // Delete cache when products are added or deleted even when in "tag-all" mode
        $this->addMetaItems($products, $expire, $tags, ['product']);


        $view->listProductItems = $products;
        $view->listProductSort = $sort;
        $view->listProductTotal = $total;

        $view->listPageSize = $size;
        $view->listPageCurr = $page;
        $view->listPagePrev = ($page > 1 ? $page - 1 : 1);
        $view->listPageLast = ($total != 0 ? ceil($total / $size) : 1);
        $view->listPageNext = ($page < $view->listPageLast ? $page + 1 : $view->listPageLast);

        $view->listParams = $this->getClientParams(map($view->param())->toArray());

        return parent::addData($view, $tags, $expire);
    }
}

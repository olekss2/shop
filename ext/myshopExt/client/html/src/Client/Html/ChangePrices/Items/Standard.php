<?php

namespace Aimeos\Client\Html\ChangePrices\Items;

use Aimeos\Client\Html\Exception;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Object_;

class Standard
    extends \Aimeos\Client\Html\Catalog\Base
    implements \Aimeos\Client\Html\Common\Client\Factory\Iface
{
    private $subPartPath = 'client/html/changeprices/items/standard/subparts';
    private $subPartNames = [];

    public function getBody(string $uid = ''): string
    {
        $view = $this->getView();

        $html = '';
        foreach ($this->getSubClients() as $subclient) {
            $html .= $subclient->setView($view)->getBody($uid);
        }
        $view->itemsBody = $html;
        $tplconf = 'client/html/changeprices/items/standard/template-body';
        $default = 'changeprices/items-body-standard';

        return $view->render($this->getTemplatePath($tplconf, $default));
    }

    public function getHeader(string $uid = ''): ?string
    {
        $view = $this->getView();

        $html = '';
        foreach ($this->getSubClients() as $subclient) {
            $html .= $subclient->setView($view)->getHeader($uid);
        }
        $view->itemsHeader = $html;
        $tplconf = 'client/html/changeprices/items/standard/template-header';
        $default = 'changeprices/items-header-standard';

        return $view->render($this->getTemplatePath($tplconf, $default));
    }

    public function getSubClient(string $type, string $name = null): \Aimeos\Client\Html\Iface
    {
        return $this->createSubClient('changeprices/items/' . $type, $name);
    }

    protected function getSubClientNames(): array
    {
        return $this->getContext()->getConfig()->get($this->subPartPath, $this->subPartNames);
    }

    public function modifyBody(string $content, string $uid): string
    {
        $content = parent::modifyBody($content, $uid);

        return $this->replaceSection($content, $this->getView()->csrf()->formfield(), 'changeprices.items.csrf');
    }

    public function addData(\Aimeos\MW\View\Iface $view, array &$tags = [], string &$expire = null): \Aimeos\MW\View\Iface
    {
        $productItems = map();
        $context = $this->getContext();
        $config = $context->getConfig();
        $products = $view->get('listProductItems', []);

        if ($config->get('client/html/changeprices/basket-add', false)) {
            foreach ($products as $product) {
                if ($product->getType() === 'select') {
                    $productItems->union($product->getRefItems('product', 'default', 'default'));
                }
            }

            $this->addMetaItems($productItems->toArray(), $expire, $tags);

            $view->itemsProductItems = $productItems;
        }
        if (!$products->isEmpty() && (bool)$config->get('client/html/changeprices/stock/enable', true) === true) {
            $view->itemsStockUrl = $this->getStockUrl($view, $products->copy()->union($productItems));
        }

        if (in_array('navigator', $config->get('client/html/changeprices/stage/standard/subparts', ['navigator']))) {
            $size = $config->get('client/html/changeprices/size', 48);
            $size = min(max($view->param('l_size', $size), 1), 100);
            $page = min(max($view->param('l_page', 1), 1), 100);

            $view->itemPosition = ($page - 1) * $size;
        }

        $customer = \Aimeos\Controller\Frontend::create($context, 'customer')->get();
        $userName = $customer->get('customer.code');
        $newProducts = [];
        foreach ($view->get('listProductItems', []) as $id => $productItem) :

            $suppliers = $productItem->get('.supplier');
            if (!($suppliers === null)) {
                foreach ($suppliers as $supplier) {
                    if ($supplier->get('supplier.code') === $userName) {
                        $newProducts[$id] = $productItem;
                        continue;
                    }
                }
            }

            $productConfig = $productItem->get('product.config');
            $allowEdit = false;
            foreach ($productConfig as $property => $configProperty):
                if (substr($property, 0, 7) === "Editor_" && $configProperty === $userName)
                    $allowEdit = true;
            endforeach;

            if (!$allowEdit)
                continue;

            $newProducts[$id] = $productItem;
        endforeach;

        $view->listProductItems = $newProducts;

        $customers = DB::table('users_list')->join('users', 'users.id', '=', 'users_list.parentid')->
        select('users.id', 'users.name')->where('users_list.refid', "=", '3')->get()->all();

        $customerIDs = [];
        $customerMap = [];
        foreach ($customers as $customer) {
            array_push($customerIDs, $customer->id);
            $customerMap[$customer->id] = $customer->name;
        }

        $productCodes = [];
        foreach ($view->get('listProductItems') as $productItem) {
            array_push($productCodes, $productItem->get('product.id'));
        }

        $discountsDB = DB::table('discounts')->whereIn('product_id', $productCodes)->
        whereIn('customer_id', $customerIDs)->get()->sortBy('customer_id')->all();

        $newDiscountEntries = [];
        foreach ($productCodes as $productCode) {
            foreach ($customerIDs as $customerID) {
                $keyFound = false;
                foreach ($discountsDB as $discount) {
                    if ($discount->product_id === intval($productCode) && $discount->customer_id === intval($customerID))
                        $keyFound = true;
                }
                if (!($keyFound))
                    array_push($newDiscountEntries, ['product_id' => $productCode,
                        'customer_id' => $customerID, 'discount' => 0]);

            }
        }

        if (!(empty($newDiscountEntries)))
            DB::table('discounts')->insert($newDiscountEntries);

        $oldDiscountEntries = [];
        $newDiscountEntriesToMerge = [];
        foreach ($discountsDB as $discount) {
            $discount->customerName = $customerMap[$discount->customer_id];
            array_push($oldDiscountEntries, (array)$discount);
        }

        foreach ($newDiscountEntries as $discount) {
            $discount->customerName = $customerMap[$discount->customer_id];
            array_push($newDiscountEntriesToMerge, (array)$discount);
        }

        $view->productDiscounts = array_merge($oldDiscountEntries, $newDiscountEntriesToMerge);

        return parent::addData($view, $tags, $expire);
    }

}

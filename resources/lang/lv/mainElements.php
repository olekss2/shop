<?php

return [
    'home' => 'Sākums',
    'products' => 'Produkti',
    'shop' => 'Preču katalogs',
    'allProducts' => 'Visi produkti',
    'allCategories' => 'Visas kategorijas',
    'allShops' => 'Visi piegādātāji',
    'about' => 'Par mums',
    'changestocks' => 'Mainīt daudzumu',
    "changeprices" => 'Mainīt cenu',
    'login' => 'Ieiet',
    'register' => 'Reģistrēties',
    'logout' => 'Izrakstīties',
    'orderTemplates' => 'Sagataves',
    'debtLimits' => 'Parādu limiti',
    'forSuppliers' => 'Piegādātājiem'
];

@extends('shop::base')

@section('aimeos_header')
<?= $aiheader['locale/select'] ?? '' ?>
<?= $aiheader['basket/mini'] ?? '' ?>
<?= $aiheader['catalog/home'] ?? '' ?>
<?= $aiheader['homepage'] ?? '' ?>
@stop

@section('aimeos_head')
<?= $aibody['locale/select'] ?? '' ?>
<?= $aibody['basket/mini'] ?? '' ?>
@stop

@section('aimeos_body')
<?= $aibody['homepage'] ?? '' ?>
<?= $aibody['catalog/home'] ?? '' ?>
@stop
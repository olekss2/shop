<?php

Auth::routes();

Route::get('/products', '\Aimeos\Shop\Controller\CatalogController@listAction')->name('aimeos_home');
Route::get('/shops', 'AllShopsController@index')->name('aimeos_home');
Route::get('/categories', 'CategoriesController@index')->name('aimeos_home');
Route::get('/about', 'AboutController@homeAction')->name('aimeos_home');
Route::get('/changeprices', 'ChangePricesController@index')->name('aimeos_home');
Route::get('/changestocks', 'ChangeStocksController@index')->name('aimeos_home');
Route::get('/orderTemplates', 'OrderTemplatesController@index')->name('aimeos_home');
Route::get('/debtLimits', 'DebtLimitsController@index')->name('aimeos_home');
Route::get('/', 'HomePageController@homeAction')->name('aimeos_home');
Route::get('/{lang}', function ($lang) {
    App::setlocale($lang);
    return redirect('/');
});

Route::post('/changestocks', 'ChangeStocksController@index')->name('aimeos_home');
Route::post('/changeprices', 'ChangePricesController@updateDiscounts')->name('aimeos_home');
Route::post('/orderTemplates', 'OrderTemplatesController@index')->name('aimeos_home');
Route::post('/debtLimits', 'DebtLimits@index')->name('aimeos_home');
